#include <iostream>

class Num {
 public:
  int sign = 1;
  void Solve(Num& ch);
  void Print() const;

  Num(const std::string& str) {
    int a = 0;
    int b = 0;
    int h = 1;
    int lf = 0;
    bool checker = true;

    for (int i = 0; i < str.size(); ++i) {
      if (str[i] == '-') {
        h = -1;
      } else {
        if (checker and str[i] != ',') {
          a = a * 10 + ((int)str[i] - 48);
        } else if (checker and str[i] == ',') {
          checker = false;
        } else if (not checker) {
          lf += 1;
          b = b * 10 + ((int)str[i] - 48);
        }
      }
    }
    while (b % 10 == 0 and b != 0) {
      b = b / 10;
      --lf;
    }
    whole_part = a * h;
    fract_part = b;
    len_fract = lf;
    sign = h;
  }

  ~Num() = default;

 private:
  int whole_part = 1;
  int fract_part = 0;
  int len_fract = 0;

  static int Pow_(int x, int y);
};

void Num::Solve(Num& ch) {
  if (this->len_fract < ch.len_fract and this->len_fract != 0) {
    this->fract_part *= Pow_(10, (ch.len_fract - this->len_fract));
    this->len_fract = ch.len_fract;

  } else if (this->len_fract > ch.len_fract and ch.len_fract != 0) {
    ch.fract_part *= Pow_(10, (this->len_fract - ch.len_fract));
    ch.len_fract = this->len_fract;
  }

  if (this->whole_part > ch.whole_part) {
    std::cout << ">" << std::endl;
  } else if (this->whole_part < ch.whole_part) {
    std::cout << "<" << std::endl;
  }

  if (this->whole_part == ch.whole_part) {
    if (this->fract_part * this->sign > ch.fract_part * ch.sign) {
      std::cout << ">" << std::endl;
    } else if (this->fract_part * this->sign < ch.fract_part * ch.sign) {
      std::cout << "<" << std::endl;
    }

    if (this->fract_part * this->sign == ch.fract_part * ch.sign) {
      std::cout << "=" << std::endl;
    }
  }
}

void Num::Print() const { std::cout << whole_part << "," << fract_part << std::endl; }

int Num::Pow_(int x, int y) {
  int ans = x;
  if (y == 0) return 1;
  for (int i = 0; i < y; ++i) {
    ans *= x;
  }
  return ans;
}

void task1() {
  std::string st1, st2;

  std::cin >> st1;
  std::cin >> st2;

  Num ch1(st1), ch2(st2);
  //  ch1.Print();
  //  ch2.Print();
  ch1.Solve(ch2);
  //  ch1.Print();
  //  ch2.Print();
}

int main() { task1(); }